package luxisedech.dreemchallenge.common.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.AndroidSupportInjection
import luxisedech.dreemchallenge.common.extensions.observeValue
import luxisedech.dreemchallenge.common.viewmodels.StateIntentViewModel
import luxisedech.dreemchallenge.common.viewmodels.ViewModelFactory
import java.lang.reflect.ParameterizedType
import javax.inject.Inject


@Suppress("UNCHECKED_CAST")
abstract class MVIDialog<State,Intent, Binding : ViewDataBinding, VM: StateIntentViewModel<State, Intent>>
    (private val layoutId:Int, private val widthPercent:Double, private val heightPercent: Double): DialogFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    protected lateinit var viewBinding: Binding
    protected lateinit var viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModelClass: Class<VM> = ((javaClass
            .genericSuperclass as ParameterizedType?)!!.actualTypeArguments[3] as Class<VM>)
        viewModel = ViewModelProvider(requireActivity().viewModelStore,viewModelFactory).get(viewModelClass)

        requireActivity().onBackPressedDispatcher
            .addCallback(this, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    backButtonAction()
                }
            })
    }
    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewBinding = DataBindingUtil.inflate(
            inflater,
            layoutId,
            container,
            false
        )

        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return viewBinding.root
    }

    fun hideKeyboard() {
        val imm = requireContext().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireView().windowToken, 0)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.lifecycleOwner = viewLifecycleOwner
        setUpViews()

        if(!viewModel.state.hasActiveObservers()) {
            viewModel.state.observeValue(viewLifecycleOwner,::renderState)
        }
    }
    override fun onResume() {
        super.onResume()
        val width = (resources.displayMetrics.widthPixels * widthPercent).toInt()
        val height = (resources.displayMetrics.heightPixels * heightPercent).toInt()
        dialog!!.window!!.setLayout(width,height)
    }
    protected abstract fun setUpViews()
    protected abstract fun renderState(viewState: State)
    protected abstract fun backButtonAction()
}