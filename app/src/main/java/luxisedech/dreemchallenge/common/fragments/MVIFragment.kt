package luxisedech.dreemchallenge.common.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import luxisedech.dreemchallenge.common.extensions.observeValue
import luxisedech.dreemchallenge.common.viewmodels.StateIntentViewModel
import kotlinx.coroutines.*
import luxisedech.dreemchallenge.common.viewmodels.ViewModelFactory
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass


@Suppress("UNCHECKED_CAST")
abstract class MVIFragment<State, Intent, Binding : ViewDataBinding, VM: StateIntentViewModel<State, Intent>>(
    private val layoutId: Int,
    private val viewModelVariable: Int,
    private val viewModelClass: KClass<VM>,
    private val pagerFragment : Boolean = false
) : Fragment(), CoroutineScope {

    protected lateinit var viewModel: VM

    override val coroutineContext: CoroutineContext
        get() = Job() + Dispatchers.Default
    private var _binding: Binding? = null
    protected val viewBinding: Binding
        get() = _binding
            ?: throw IllegalStateException("Trying to access the binding outside of the view lifecycle.")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(viewModelClass.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = DataBindingUtil.inflate<Binding>(inflater, layoutId, container, false)
        .also { _binding = it }.root


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.lifecycleOwner = viewLifecycleOwner
        viewBinding.setVariable(viewModelVariable, viewModel)
    }

    override fun onStart() {
        super.onStart()
        if (!pagerFragment) {
            setupViews()
        }
        if(!viewModel.state.hasActiveObservers()) {
            viewModel.state.observeValue(viewLifecycleOwner,::renderState)
        }
    }

    fun Intent.dispatchIt() = dispatch(this)
    fun dispatch(intent: Intent) = viewModel.dispatchIntent(intent)

    private var request: Job? = null
    override fun onResume() {
        super.onResume()
        if (pagerFragment) {
            setupViews()
        }
        request = launch { scheduledAction() }
    }

    open suspend fun scheduledAction() {}
    override fun onPause() {
        super.onPause()
        request?.cancel()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    protected abstract fun setupViews()
    protected abstract fun renderState(viewState: State)
}
