package luxisedech.dreemchallenge.common.viewmodels

import androidx.lifecycle.LiveData
import kotlinx.coroutines.Job

interface IModel<STATE, INTENT> {

    val state: LiveData<STATE>

    fun dispatchIntent(intent: INTENT): Job
}