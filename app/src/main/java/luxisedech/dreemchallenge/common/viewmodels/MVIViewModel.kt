package luxisedech.dreemchallenge.common.viewmodels

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel

abstract class MVIViewModel<State, Intent, PartialState> constructor(
    stateReducer: IReducer<State, PartialState>,
    initialAction:(() -> Unit)? = null
) :
    StateIntentViewModel<State, Intent>() {

    private val stateChannel = Channel<PartialState>()

    protected suspend fun reduce(partialState: PartialState) =
        stateChannel.send(partialState)

    protected suspend fun PartialState.reduceIt() = reduce(this)

    private val reducer: IReducer<State, PartialState> =
        stateReducer

    init {
        launch(Dispatchers.Main) {
            for (partialState in stateChannel) {
                _state.value = reducer.reduce(_state.value, partialState)
            }
        }
        if (initialAction != null) {
            launch {
                initialAction()
            }
        }
    }

    override fun dispatchIntent(intent: Intent) =
        launch {
            intentToAction(intent)

        }


    protected abstract suspend fun intentToAction(intent: Intent) : Any

}

