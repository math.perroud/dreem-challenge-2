package luxisedech.dreemchallenge.common.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

abstract class StateIntentViewModel<State,Intent>: ViewModel(), IModel<State, Intent>, CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Job() + Dispatchers.Default

    protected val _state =
        NonNullMutableLiveData(createInitialState())
    override val state: LiveData<State>
        get() = _state

    abstract fun createInitialState() : State
    abstract override fun dispatchIntent(intent: Intent) : Job
}
