package luxisedech.dreemchallenge.data.repositories

import luxisedech.dreemchallenge.data.sources.local.cache.UserCache
import luxisedech.dreemchallenge.data.sources.remote.api.models.UserDtoIn
import luxisedech.dreemchallenge.data.sources.remote.api.services.UserService
import luxisedech.dreemchallenge.domain.Either

object UserRepository {
    private lateinit var userCache: UserCache
    private lateinit var userService: UserService

    operator fun invoke(/*context: Context*/): UserRepository {
        /*userCache = UserCache(context)*/
        userService = UserService()
        return this
    }

    suspend fun getAllUsers(): Either<List<UserDtoIn>, Exception> =
        userService.fetchRemoteUsers()

}