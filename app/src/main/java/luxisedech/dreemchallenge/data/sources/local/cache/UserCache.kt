package luxisedech.dreemchallenge.data.sources.local.cache

import android.content.Context
import androidx.room.Room
import luxisedech.dreemchallenge.data.sources.local.room.LocalDatabase
import luxisedech.dreemchallenge.data.sources.remote.api.models.UserDtoIn

object UserCache {
    private lateinit var db: LocalDatabase
    operator fun invoke(context: Context): UserCache {
        db = Room
            .databaseBuilder(context, LocalDatabase::class.java, "LocalDatabase")
            .build()
        return this
    }

    fun getAllUsers(): List<UserDtoIn> = emptyList()
}