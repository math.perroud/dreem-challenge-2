package luxisedech.dreemchallenge.data.sources.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import luxisedech.dreemchallenge.data.sources.local.room.user.DBUser
import luxisedech.dreemchallenge.data.sources.local.room.user.UserDao

@Database(entities = [
    (DBUser::class)
], version = 1)
abstract class LocalDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}
