package luxisedech.dreemchallenge.data.sources.local.room.user

import androidx.room.*

@Entity(tableName = "Users")
data class DBUser(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,

)