package luxisedech.dreemchallenge.data.sources.local.room.user

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUsers(orders: List<DBUser>)
}