package luxisedech.dreemchallenge.data.sources.remote.api

data class CreatePostCommand(
    val title: String,
    val body: String,
    val userId: Int
)
