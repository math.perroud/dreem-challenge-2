package luxisedech.dreemchallenge.data.sources.remote.api

import luxisedech.dreemchallenge.data.sources.remote.api.models.CreatePostResponse
import luxisedech.dreemchallenge.data.sources.remote.api.models.UserDtoIn
import retrofit2.Response
import retrofit2.http.*

interface TypicodeApi {

    @GET("users")
    suspend fun getUsers(): Response<List<UserDtoIn>>

    @GET("users/{id}")
    suspend fun getUserById(@Path("id") id: Int): Response<UserDtoIn>

    @POST("posts")
    suspend fun createPost(@Body  command:CreatePostCommand): Response<CreatePostResponse>

    @DELETE("posts/{userId}")
    suspend fun deletePostByUserId(@Path("userId") id: Int): Response<Any>
}
