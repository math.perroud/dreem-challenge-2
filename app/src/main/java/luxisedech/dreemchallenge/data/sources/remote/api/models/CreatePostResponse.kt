package luxisedech.dreemchallenge.data.sources.remote.api.models

data class CreatePostResponse(
    val id: Int,
    val title: String,
    val body: String,
    val userId: Int
)
