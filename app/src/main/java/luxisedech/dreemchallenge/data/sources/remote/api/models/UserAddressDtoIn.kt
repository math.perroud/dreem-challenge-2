package luxisedech.dreemchallenge.data.sources.remote.api.models

data class UserAddressDtoIn(
    val street: String,
    val suite: String,
    val city: String,
    val zipcode: String,
    val geo: UserGeoDtoIn
)
