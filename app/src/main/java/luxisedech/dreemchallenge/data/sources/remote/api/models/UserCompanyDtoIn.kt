package luxisedech.dreemchallenge.data.sources.remote.api.models


data class UserCompanyDtoIn(
    val name: String,
    val catchPhrase: String,
    val bs: String
)