package luxisedech.dreemchallenge.data.sources.remote.api.models

data class UserDtoIn(
    val id: Long,
    val name: String,
    val username: String,
    val email: String,
    val address: UserAddressDtoIn,
    val phone: String,
    val website: String,
    val company: UserCompanyDtoIn
)