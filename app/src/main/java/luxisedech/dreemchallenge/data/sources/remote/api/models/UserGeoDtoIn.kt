package luxisedech.dreemchallenge.data.sources.remote.api.models

data class UserGeoDtoIn(
    val lat: String,
    val lng: String
)
