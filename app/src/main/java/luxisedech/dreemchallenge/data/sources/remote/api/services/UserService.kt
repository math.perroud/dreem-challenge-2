package luxisedech.dreemchallenge.data.sources.remote.api.services

import com.google.gson.GsonBuilder
import luxisedech.dreemchallenge.data.sources.remote.api.TypicodeApi
import luxisedech.dreemchallenge.data.sources.remote.api.models.UserDtoIn
import luxisedech.dreemchallenge.domain.Either
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object UserService {
    private lateinit var api: TypicodeApi
    operator fun invoke(): UserService {
        api =
            Retrofit.Builder()
                .addConverterFactory(
                    GsonConverterFactory.create(
                        GsonBuilder()
                            .create()
                    )
                )
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(TypicodeApi::class.java)
        return this
    }

    suspend fun fetchRemoteUsers(): Either<List<UserDtoIn>, Exception> {
        with(api.getUsers()) {
            return if (this.isSuccessful) {
                body()?.let {
                    Either.Success(it)
                } ?: Either.Failure(Exception())
            } else {
                Either.Failure(Exception("${this.message()} : ${this.code()}"))
            }
        }
    }
}