package luxisedech.dreemchallenge.domain

sealed class Either<out S : Any, out F : Exception> {

    data class Success<S : Any>(val content: S) : Either<S, Nothing>()
    data class Failure<F : Exception>(val content: F) : Either<Nothing, F>()

    suspend fun <ReturnType> either(onSuccess:suspend ((S) -> ReturnType), onFailure: suspend ((F) -> ReturnType)) : ReturnType=
        when (this) {
            is Success<S> -> onSuccess.invoke(content)
            is Failure<F> -> onFailure.invoke(content)
        }


}