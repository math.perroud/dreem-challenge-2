package luxisedech.dreemchallenge.screens.fragments.home

import luxisedech.dreemchallenge.BR
import luxisedech.dreemchallenge.R
import luxisedech.dreemchallenge.common.fragments.MVIFragment
import luxisedech.dreemchallenge.databinding.FragmentHomeBinding
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*


class HomeFragment :
    MVIFragment<HomeState, HomeIntent, FragmentHomeBinding, HomeViewModel>(
        R.layout.fragment_home, BR.viewmodel, HomeViewModel::class
    ) {

    override fun setupViews() {
        HomeIntent.LoadUsers.dispatchIt()
        with(viewBinding) {
            val output = SimpleDateFormat("MMMM dd,yyy", Locale.getDefault())
            val result = output.format(Date().time)
            lblDate.text = result
        }
    }

    override fun renderState(viewState: HomeState) {
        viewState.triggeredEvents.onEach { event ->
            when (event) {

            }
            viewState.triggeredEvents.poll()
        }
    }


}