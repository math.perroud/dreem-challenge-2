package luxisedech.dreemchallenge.screens.fragments.home

import luxisedech.dreemchallenge.data.sources.remote.api.models.UserDtoIn

sealed class HomeIntent {
    object LoadUsers : HomeIntent()
    data class ClickedOnUser(val user: UserDtoIn) : HomeIntent()
}