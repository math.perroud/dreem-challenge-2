package luxisedech.dreemchallenge.screens.fragments.home

import luxisedech.dreemchallenge.data.sources.remote.api.models.UserDtoIn

sealed class HomePartialState {
    data class DisplayUsers(val users: List<UserDtoIn>) : HomePartialState()
}