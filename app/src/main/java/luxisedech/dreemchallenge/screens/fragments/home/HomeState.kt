package luxisedech.dreemchallenge.screens.fragments.home

import java.util.*
import java.util.concurrent.ArrayBlockingQueue

data class HomeState (
    val triggeredEvents: Queue<Event> = ArrayBlockingQueue(10)
    ){
    sealed class Event {

    }
}