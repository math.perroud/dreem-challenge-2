package luxisedech.dreemchallenge.screens.fragments.home

import luxisedech.dreemchallenge.common.viewmodels.MVIViewModel
import luxisedech.dreemchallenge.data.repositories.UserRepository

class HomeViewModel : MVIViewModel<HomeState, HomeIntent, HomePartialState>
    (HomeReducer()) {

    override fun createInitialState() = HomeState()

    override suspend fun intentToAction(intent: HomeIntent) =
        when (intent) {
            is HomeIntent.ClickedOnUser -> {
            }
            is HomeIntent.LoadUsers -> fetchUsers()
        }

    private suspend fun fetchUsers() {
        UserRepository().getAllUsers().either(
            onSuccess = {
                HomePartialState.DisplayUsers(it).reduceIt()
            },
            onFailure = {

            }
        )
    }
}
