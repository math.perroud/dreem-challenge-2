package luxisedech.dreemchallenge.screens.fragments.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import luxisedech.dreemchallenge.R
import luxisedech.dreemchallenge.data.sources.remote.api.models.UserDtoIn
import luxisedech.dreemchallenge.databinding.ListItemUserBinding
import kotlin.collections.ArrayList


class UsersRecyclerAdapter(val onUserClick:(UserDtoIn)->Unit) :
    RecyclerView.Adapter<UsersRecyclerAdapter.ViewHolder>() {

    private var itemList = ArrayList<UserDtoIn>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_item_user,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(itemList[position],onUserClick)
    }

    override fun getItemCount(): Int = itemList.size


    fun updateDataList(newData: List<UserDtoIn>) {
        itemList = ArrayList(newData)
        notifyDataSetChanged()
    }

    class ViewHolder(private val itemBinding: ListItemUserBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(user: UserDtoIn, onClick:(UserDtoIn)->Unit) {
            itemView.setOnClickListener { onClick.invoke(user) }
            with(itemBinding) {

                lblName.text = user.name
                lblUsername.text = user.username
                executePendingBindings()
            }
        }

    }
}
